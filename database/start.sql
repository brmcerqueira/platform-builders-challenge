create sequence customer_seq;

create table if not exists customer(
   id bigint not null default nextval ('customer_seq'),
   cpf varchar(11) not null,
   dateBirth date not null,
   name varchar(50) not null,
   primary key (id)
)