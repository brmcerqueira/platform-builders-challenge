# Platform Builders Challenge #

Desafio da Platform Builders

# Guia
- [Requisitos](#requisitos)
- [Rodando a aplicação com o Docker](#rodando-a-aplicação-com-o-docker)

# Requisitos

- Gradle
- Docker
- Docker Compose

# Rodando a aplicação com o Docker

Para executar a aplicação precisamos montala primeiro, Para isso vamos abrir um terminal na raiz do projeto e executar o seguinte comando

``# gradle bootjar``

Após isso precisamos levantar o Docker Compose, no mesmo terminal vamos executar o seguinte comando 

``# docker-compose up -d``

Nesse momento a aplicação deve esta rodando normalmente.

Se achar necessario podemos observar o log da aplicação com o seguinte comando 

``# docker logs platform-builders-challenge -f``