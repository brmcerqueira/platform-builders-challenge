FROM openjdk:11

COPY ./build/libs ./applications

WORKDIR /applications

EXPOSE 8080

ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar -Dspring.profiles.active=docker platform-builders-challenge-1.0-SNAPSHOT.jar"]