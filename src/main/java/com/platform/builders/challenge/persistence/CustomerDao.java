package com.platform.builders.challenge.persistence;

import com.platform.builders.challenge.domain.Customer;
import com.platform.builders.challenge.dto.CustomerSaveDto;
import com.platform.builders.challenge.dto.CustomerSearchDto;
import com.platform.builders.challenge.dto.CustomerUpdateDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.util.List;

@Mapper
@Repository
public interface CustomerDao {
    @Insert("insert into customer(name, cpf, dateBirth) values(#{dto.name}, #{dto.cpf}, #{dto.dateBirth})")
    void create(@Param("dto") CustomerSaveDto dto);

    @Update("update customer set name = #{dto.name}, cpf = #{dto.cpf}, dateBirth = #{dto.dateBirth} where id = #{id}")
    void replace(@Param("id") long id, @Param("dto") CustomerSaveDto dto);

    @Update({"<script>",
            "update customer",
            "  <set>",
            "    <if test='dto.name != null'>name = #{dto.name},</if>",
            "    <if test='dto.cpf != null'>cpf = #{dto.cpf},</if>",
            "    <if test='dto.dateBirth != null'>dateBirth = #{dto.dateBirth}</if>",
            "  </set>",
            "where id = #{id}",
            "</script>"})
    void update(@Param("id") long id, @Param("dto") CustomerUpdateDto dto);

    @Select("select * from customer where id = #{id}")
    Customer find(@Param("id") long id);

    @Delete("delete from customer where id = #{id}")
    void remove(@Param("id") long id);

    @Select({"<script>",
            "select * from customer",
            "  <where>",
            "    <if test='dto.name != null'>name like '%' || #{dto.name} || '%'</if>",
            "    <if test='dto.cpf != null'>AND cpf = #{dto.cpf}</if>",
            "  </where>",
            "<if test='dto.limit != null'>limit #{dto.limit}</if>",
            "<if test='dto.index != null'>offset #{dto.index}</if>",
            "</script>"})
    List<Customer> search(@Param("dto") CustomerSearchDto dto);
}