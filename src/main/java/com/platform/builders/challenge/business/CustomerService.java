package com.platform.builders.challenge.business;

import com.platform.builders.challenge.domain.Customer;
import com.platform.builders.challenge.dto.CustomerSaveDto;
import com.platform.builders.challenge.dto.CustomerSearchDto;
import com.platform.builders.challenge.dto.CustomerSearchOutputDto;
import com.platform.builders.challenge.dto.CustomerUpdateDto;
import com.platform.builders.challenge.persistence.CustomerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    private final CustomerDao dao;

    @Autowired
    public CustomerService(CustomerDao dao) {
        this.dao = dao;
    }

    public Customer find(long id) {
        return this.dao.find(id);
    }

    public void remove(long id) {
        this.dao.remove(id);
    }

    public void create(CustomerSaveDto dto) {
        this.dao.create(dto);
    }

    public void replace(long id, CustomerSaveDto dto) {
        this.dao.replace(id, dto);
    }

    public void update(long id, CustomerUpdateDto dto) {
        this.dao.update(id, dto);
    }

    public List<CustomerSearchOutputDto> search(CustomerSearchDto dto) {
        return this.dao.search(dto).stream().map(item -> {
            CustomerSearchOutputDto outputDto = new CustomerSearchOutputDto();
            outputDto.setId(item.getId());
            outputDto.setName(item.getName());
            outputDto.setCpf(item.getCpf());
            outputDto.setDateBirth(item.getDateBirth());
            outputDto.setAge(Period.between(item.getDateBirth(), LocalDate.now()).getYears());
            return outputDto;
        }).collect(Collectors.toList());
    }
}