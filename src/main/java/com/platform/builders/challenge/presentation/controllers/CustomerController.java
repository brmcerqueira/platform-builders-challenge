package com.platform.builders.challenge.presentation.controllers;

import com.platform.builders.challenge.business.CustomerService;
import com.platform.builders.challenge.domain.Customer;
import com.platform.builders.challenge.dto.CustomerSaveDto;
import com.platform.builders.challenge.dto.CustomerSearchDto;
import com.platform.builders.challenge.dto.CustomerSearchOutputDto;
import com.platform.builders.challenge.dto.CustomerUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/rest")
public class CustomerController {
    private final CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @RequestMapping(value="/customer", method = RequestMethod.POST)
    public void create(@Valid @RequestBody CustomerSaveDto dto) {
        this.service.create(dto);
    }

    @RequestMapping(value="/customer/{id}", method = RequestMethod.PUT)
    public void replace(@PathVariable long id, @Valid @RequestBody CustomerSaveDto dto) {
        this.service.replace(id, dto);
    }

    @RequestMapping(value="/customer/{id}", method = RequestMethod.PATCH)
    public void update(@PathVariable long id, @Valid @RequestBody CustomerUpdateDto dto) {
        this.service.update(id, dto);
    }

    @RequestMapping(value="/customer", method = RequestMethod.GET)
    public List<CustomerSearchOutputDto> search(CustomerSearchDto dto) {
        return this.service.search(dto);
    }

    @RequestMapping(value="/customer/find/{id}", method = RequestMethod.GET)
    public Customer find(@PathVariable long id) {
        return this.service.find(id);
    }

    @RequestMapping(value="/customer/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable long id) {
        this.service.remove(id);
    }
}